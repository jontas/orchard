'use strict';

var express = require('express'),
	http = require('http'),
	app = express(),
	_ = require('lodash'),
	path = require('path'),
	mysql = require('mysql'),
	q = require('q'),
	dateformat = require('dateformat');

app.set('views', path.join(__dirname, 'templates'));
app.set('view engine', 'jade');
app.use(express.static('public'));

var db = mysql.createConnection({
	host: 'localhost',
	user: 'orchard',
	password: 'orchard',
	database: 'orchard'
});
db.connect();

// Handle requests for json data
app.get('/get-businesses', function(req, res) {

	// Build the SQL query based on filters, limit, and order by
	var filters = req.query.data || {}
	var where = '';

	if (filters.name) {
		where += ' AND b.dba LIKE ' + db.escape('%' + filters.name + '%');
	}

	if (filters.boro) {
		where += ' AND br.id = ' + parseInt(filters.boro, 10);
	}

	if (filters.cuisine) {
		where += ' AND c.id = ' + parseInt(filters.cuisine, 10);
	}

	if (filters.zip) {
		var zips = filters.zip.split(',');
		for (var i in zips) {
			zips[i] = db.escape(_.trim(zips[i]));
		}
		where += ' AND b.zipcode IN (' + zips.join(',') + ') ';
	}

	if (filters.grade) {
		where += ' AND b.grade <= ' + parseInt(filters.grade, 10);
	}

	// where += ' AND b.grade <> 5 '; // ignore records without any grade

	var start = req.query.start ? parseInt(req.query.start, 10) : 0;
	var length = req.query.length ? parseInt(req.query.length, 10) : 10;
	var limit = start + ',' + length;

	var order;
	var order_dir = !req.query.order || req.query.order[0].dir == 'asc' ? ' ASC ' : ' DESC ';

	switch (parseInt(req.query.order ? req.query.order[0].column : 0, 10)) {
		case 0:
		case 1:
		case 4:
			order = 'b.dba' + order_dir + ', b.score ASC';
			break;
		case 2:
			order = 'br.boro' + order_dir + ', b.dba ASC';
			break;
		case 3:
			order = 'b.zipcode' + order_dir + ', b.dba ASC';
			break;
		case 5:
			order = 'c.cuisine' + order_dir + ', b.dba ASC';
			break;
		case 6:
			order = 'b.grade' + order_dir + ', b.score' + order_dir + ', b.dba ASC';
			break;
		case 7:
			order = 'b.inspection_date' + order_dir + ', b.dba ASC';
			break;
	}

	var sql = 'SELECT ' +
				'SQL_CALC_FOUND_ROWS ' +
				'IF(b.dba = "", "NONE", b.dba) as dba, ' +
				'CONCAT(b.building, " ", b.street) as address,' +
				'br.boro, ' +
				'b.zipcode,' +
				'IF(LENGTH(b.phone) <> 10, "N/A", b.phone) as phone,' +
				'c.cuisine, ' +
				'IF(b.grade = 1, "A", IF(b.grade = 2, "B", IF(b.grade = 3, "C", IF(b.grade = 4, "Not Yet Graded", IF(b.grade = 5, "Grade Pending", "Grade Missing"))))) as `grade`, ' +
				'b.inspection_date ' +
			'FROM ' +
				'businesses b ' +
			'INNER JOIN cuisines c ' +
				'ON b.cuisine_id = c.id ' +
			'INNER JOIN boros br ' +
				'ON b.boro_id = br.id ' +
			'WHERE 1 ' + where +
			' ORDER BY ' + order +
			' LIMIT ' + limit;

	// console.log(sql);

	var data = {
		data: []
	};

	db.query(sql, function(err, rows) {
		db.query('SELECT FOUND_ROWS()', function(err, count) {
			// This is needed for pagination
			data.recordsTotal = data.recordsFiltered = count.pop()['FOUND_ROWS()'];

			for (var i in rows) {
				var row = rows[i];
				data.data.push([
					row.dba,
					row.address,
					row.boro,
					row.zipcode,
					row.phone.replace(/(\d{3})(\d{3})(\d{4})/, '($1) $2-$3'),
					row.cuisine,
					row.grade,
					(row.inspection_date == 'Mon Jan 01 1900 00:00:00 GMT+0000 (UTC)' ? 'N/A' : dateformat(row.inspection_date, 'm/d/yyyy'))
				]);
			}

			res.json(data);
		});
	});
});


// Handle the main index route
app.get('/', function(req, res) {

	var data = {
		boros: {},
		cuisines: {}
	};

	function get_boros() {
		var deferred = q.defer();
		db.query('SELECT * FROM boros', function(err, rows) {
			deferred.resolve(rows);
		});
		return deferred.promise;
	}

	function get_cuisines() {
		var deferred = q.defer();
		db.query('SELECT * FROM cuisines', function(err, rows) {
			deferred.resolve(rows);
		});
		return deferred.promise;
	}

	q.all([
		get_boros(),
		get_cuisines()
	]).then(function(results) {
		for (var i in results[0]) {
			var row = results[0][i];
			if (row.boro == 'Missing') {
				continue;
			}
			row.boro = _.capitalize(row.boro.toLowerCase());
			data.boros[row.id] = row.boro;
		}

		for (var i in results[1]) {
			var row = results[1][i]
			data.cuisines[row.id] = row.cuisine;
		}
 		res.render('index', data);
	});
});

var server = app.listen(3000);