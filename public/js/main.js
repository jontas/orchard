(function($) {
	$(function() {
		var table = $('#results').DataTable({
			'processing': true,
			'serverSide': true,
			'searching' : false,
			'ajax': {
				url: '/get-businesses',
				data: function(d) {
					d.data = {
						name: $('#name').val(),
						boro: $('#boro').val(),
						zip: $('#zip').val(),
						cuisine: $('#cuisine').val(),
						grade: $('#grade').val()
					}
				}
			},
			columns: [
				{name: 'dba', searchable: false},
				{name: 'address', orderable: false, searchable: false},
				{name: 'boro', searchable: false},
				{name: 'zipcode', searchable: false},
				{name: 'phone', orderable: false, searchable: false},
				{name: 'cuisine', searchable: false},
				{name: 'grade', searchable: false},
				{name: 'inspection_date', searchable: false}
			]
		});

		$('#quick').click(function() {
			var thai_id = $('#cuisine option').filter(function() { return $(this).text() == 'Thai' }).attr('value');
			$('#name').val('');
			$('#boro').val('');
			$('#zip').val('');
			$('#cuisine').val(thai_id);
			$('#grade').val(2);
			$('select[name="results_length"]').val(10).trigger('change');
			// table.ajax.reload();
			return false;
		});

		$('#filters input, #filters select').change(function() {
			table.ajax.reload();
		});
	});
})(jQuery);