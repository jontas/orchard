DROP TABLE IF EXISTS `businesses`;
CREATE TABLE `businesses` (
    `id` int(10) unsigned NOT NULL,
    `dba` varchar(255) NOT NULL default '',
    `boro_id` int(10) unsigned NOT NULL,
    `building` varchar(255) NOT NULL,
    `street` varchar(255) NOT NULL,
    `zipcode` char(5) NOT NULL,
    `phone` char(10) NOT NULL,
    `cuisine_id` int(10) unsigned NOT NULL,
    `score` tinyint(2) unsigned NOT NULL,
    `grade` tinyint(2) unsigned NOT NULL,
    `inspection_date` date NOT NULL,
    PRIMARY KEY (`id`),
    INDEX `score_idx` (`score`),
    INDEX `grade_idx` (`grade`)
) ENGINE=innodb CHARSET=utf8;

DROP TABLE IF EXISTS `boros`;
CREATE TABLE `boros` (
	`id` int(10) unsigned NOT NULL auto_increment,
	`boro` varchar(255),
	PRIMARY KEY (`id`)
) ENGINE=innodb CHARSET=utf8;

DROP TABLE IF EXISTS `cuisines`;
CREATE TABLE `cuisines` (
	`id` int(10) unsigned NOT NULL auto_increment,
	`cuisine` varchar(255),
	PRIMARY KEY (`id`)
) ENGINE=innodb CHARSET=utf8;