<?php

date_default_timezone_set('America/New_York');
$csv = fopen('./data.csv', 'r');
// $csv = fopen('./data_short.csv', 'r');
$db = new mysqli('localhost', 'orchard', 'orchard', 'orchard');

//clear old data
$db->query('DELETE FROM boros');
$db->query('DELETE FROM cuisines');
$db->query('DELETE FROM businesses');

$db->query(<<<'EOT'
	CREATE TEMPORARY TABLE data (
		`id` int(10) unsigned NOT NULL,
	    `dba` varchar(255) NOT NULL,
	    `boro` varchar(255) NOT NULL,
	    `building` varchar(255) NOT NULL,
	    `street` varchar(255) NOT NULL,
	    `zipcode` char(5) NOT NULL,
	    `phone` char(10) NOT NULL,
	    `cuisine` varchar(255) NOT NULL,
	    `score` tinyint(2) unsigned NOT NULL,
	    `grade` varchar(255) NOT NULL,
	    `inspection_date` date NOT NULL,
	    INDEX `id_idx` (`id`),
	    INDEX `inspection_date_idx` (`inspection_date`)
	)
EOT
);

$x = 0;
while ($row = fgetcsv($csv)) {
	if (!$x++) continue; //skip header row
	$date = date('Y-m-d', strtotime($row[8]));
	$row = array_map(array($db, 'real_escape_string'), $row);
	$db->query(<<<EOT
		INSERT INTO data
			(id, dba, boro, building, street, zipcode, phone, cuisine, inspection_date, grade, score)
		VALUES
			('{$row[0]}', '{$row[1]}', '{$row[2]}', '{$row[3]}', '{$row[4]}', '{$row[5]}', '{$row[6]}', '{$row[7]}', '{$date}', '{$row[14]}', '{$row[13]}')
EOT
	);
}

$db->query('CREATE TEMPORARY TABLE data1 LIKE data');
$db->query('INSERT data1 SELECT * FROM data');

//get cuisines and boros
$cuisines = [];
$boros = [];
$res = $db->query('SELECT DISTINCT cuisine FROM data');
while ($row = $res->fetch_object()) {
	$db->query("INSERT INTO cuisines (cuisine) VALUES ('{$row->cuisine}')");
	$cuisines[$row->cuisine] = $db->query('SELECT LAST_INSERT_ID()')->fetch_object()->{'LAST_INSERT_ID()'};
}

$res = $db->query('SELECT DISTINCT boro FROM data');
while ($row = $res->fetch_object()) {
	$db->query("INSERT INTO boros (boro) VALUES ('{$row->boro}')");
	$boros[$row->boro] = $db->query('SELECT LAST_INSERT_ID()')->fetch_object()->{'LAST_INSERT_ID()'};
}

//select the record for each unique business that has the
//most recent inspection date
$res = $db->query(<<<EOT
	SELECT data.*
	FROM data
	WHERE DATE(data.inspection_date) = (
		SELECT MAX(DATE(data1.inspection_Date))
		FROM data1
		WHERE data.id = data1.id
	) GROUP BY data.id
EOT
);

// insert the relevant record for each business
while ($row = $res->fetch_object()) {
	$boro_id = $boros[$row->boro];
	$cuisine_id = $cuisines[$row->cuisine];
	$grade = get_grade($row->grade);
	foreach ($row as $k => $v) {
		$row->$k = $db->real_escape_string($v);
	}
	$db->query(<<<EOT
		INSERT INTO businesses
			(id, dba, boro_id, building, street, zipcode, phone, cuisine_id, inspection_date, grade, score)
		VALUES
			('{$row->id}', '{$row->dba}', '{$boro_id}', '{$row->building}', '{$row->street}', '{$row->zipcode}', '{$row->phone}', '{$cuisine_id}', '{$row->inspection_date}', '{$grade}', '{$row->score}')
EOT
	);
}

$db->close();
fclose($csv);

function get_grade($grade) {
	if ($grade == 'Not Yet Graded') {
		return 4;
	}
	if ($grade == 'P' || $grade == 'Z') {
		return 5;
	}
	if ($grade == '') {
		return 6;
	}
	return ord($grade) - 64;
}