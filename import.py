import petl as etl
import pymysql as db
import multiprocessing as mp
from dateutil.parser import parse

# This is needed to fix some unicode issues in python 2.x
# This is not needed in python 3
# See: http://stackoverflow.com/questions/21129020/how-to-fix-unicodedecodeerror-ascii-codec-cant-decode-byte
import sys
reload(sys)
sys.setdefaultencoding('Cp1252')

connection = db.connect(password='orchard', user='orchard', database='orchard')
cursor = connection.cursor()
cursor.execute('SET SQL_MODE=ANSI_QUOTES')
data = etl.fromcsv('./data.csv')
# data = etl.fromcsv('./data_short.csv')  #smaller file to make testing faster

# clear old data
cursor.execute('DELETE FROM boros')
cursor.execute('DELETE FROM cuisines')
cursor.execute('DELETE FROM businesses')
connection.commit()

def store_data(data, table):
	""" Store data in the db, used by multiprocessing module """
	etl.todb(data, connection, table)
	ids.put(get_ids(table))

def get_ids(table):
	""" Returns a dictionary that looks like {value: id} for a given table """
	cursor.execute('SELECT * FROM ' + table)
	# the loop in the next line reverses the keys and values in the dict to make lookups easier
	return {'table':table, 'ids': {v : k for k, v in dict(cursor.fetchall()).items()}}

def get_grade(grade):
	""" Return the number corresponding to a letter grade (eg A=1, B=2) """
	if grade == 'Not Yet Graded':
		return 4
	elif grade == 'P' or grade == 'Z':
		return 5;
	elif grade == '':
		return 6;
	else:
		return ord(grade) - 64

# Load all the cuisines and boros into the db in parallel
to_store = [
	[etl.setheader(etl.distinct(etl.cut(data, 'CUISINE DESCRIPTION')), ['cuisine']), 'cuisines'],
	[etl.setheader(etl.distinct(etl.cut(data, 'BORO')), ['boro']), 'boros']
]

ids = mp.Queue()
processes = [mp.Process(target=store_data, args=(x)) for x in to_store]

for p in processes:
    p.start()

for p in processes:
    p.join()

results = [ids.get() for p in processes]

# reformat the results to make lookups easier
ids = {}
for result in results:
	ids[result['table']] = result['ids']

# Load all of the inspection data
businesses = etl.cut(data, 'CAMIS', 'DBA', 'BORO', 'BUILDING', 'STREET', 'ZIPCODE', 'PHONE', 'CUISINE DESCRIPTION', 'INSPECTION DATE', 'GRADE', 'SCORE')

# [1:] skips the first row of headers
for business in businesses[1:]:

	# check to see if there is already a row for this business, and if so, grab the date
	cursor.execute('SELECT inspection_date FROM businesses WHERE id = ' + business[0])
	existing_date = cursor.fetchone() or ''

	# if there is an existing date, and it is older than the new one, replace with newer data
	if (existing_date == '' or existing_date[0] < parse(business[8]).date()):
		cuisine_id = ids['cuisines'][business[7]]
		boro_id = ids['boros'][business[2]]
		cursor.execute(
			"""INSERT INTO
			businesses
				(id, dba, boro_id, building, street, zipcode, phone, cuisine_id, inspection_date, grade, score)
			VALUES
				(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)
			ON DUPLICATE KEY UPDATE
				inspection_date = VALUES(inspection_date),
				score = VALUES(score),
				grade = VALUES(grade)""",
				(business[0], business[1], boro_id, business[3], business[4], business[5], business[6], cuisine_id, parse(business[8]).strftime('%Y-%m-%d'), get_grade(business[9]), business[10] or 0)
		)
		connection.commit()