# NYC Health Grade Data Viewer #

This project was completed by Jon Tascher for The Orchard

### Schema ###

The database schema can be found in db.sql.  There are three tables:

* cuisines
* boros
* businesses

The first two are lookup tables and the third holds all the relevant data about each business.  The original datafile contained one row *per inspection*, with many restaurants appearing multiple times.  Since the task was to find a restaurant to eat at today, and not at some point in the past, I decided the historical inspection data was not relevant.  Thus, I only imported the most recent record for each business.  If the historical data had been required, I would've had another table: `inspections`, containing one row per inspection and a `business_id`.

### ETL Process ###

I built two versions of the ETL process, one in python and one in PHP.  I know that the role I am interviewing for includes a significant amount of python work, so I wanted to demonstrate that I can pick it up quickly.  However, once I had finished writing the python version I started to worry that my use of idiom was not up to snuff, so I decided to whip up a quick PHP version as well to show my skills with language I have much more experience with.  I didn't do any OOP stuff, though I know OOP methodology inside and out.  I have sent Katie some examples of OOP code I've written, and they can be viewed here:

* https://wordpress.org/plugins/request-access/
* https://wordpress.org/plugins/sailthru-triggermail/
* https://wordpress.org/plugins/wp-phototagger/

The python version uses a multithreaded ETL process.  I made use of a library called [petl](https://petl.readthedocs.org/en/latest/), which lets me slice and query the dataset without loading the whole thing into memory.  Additionally, when importing the cuisines and boroughs, I did so in parallel using the multiprocessing library.  This resulted in the python version being about 30% faster than the PHP version (on my laptop).

The petl library does not let me run queries as complex as an RDBMS does, so I wasn't able to select just the relevant rows for each business directly using the library.  To avoid having to keep too much in memory at once, I wrote each inspection record to the database as I found it, and then updated the row each time I found a new record for a given businesses with a more recent inspection date.

I handled the PHP version a little differently.  The [fgetcsv](http://php.net/manual/en/function.fgetcsv.php) function allows large files to be processed one line at a time, similar to the way the python library worked (avoids putting the whole thing into memory).  I was, however, curious as to how the speed would differ if I made use of mysql to handle locating the relevant records.  I created a temporary table, filled it with all the raw data, and then wrote a query to find the most recent inspection row for each business.  Unfortunately, due to a limitation with mysql, you cannot reference a temp table more than once in a single query.  To get around this, I used two temp tables, though this is not the sort of hack I'd use in production code.  The PHP version is not threaded:  I have done plenty of threading in PHP and it is always irritating--I was pleased by how easily python handled it.  Since the PHP import script pulls only the relevant rows from the temp table, they are then just inserted into the database, no UPDATE statements are needed.

### Front End ###

I could've whipped up the server side component for the front end in PHP in my sleep, but decided to have a little more fun and do it in node.js.  I worked in node for about 9 months in my last role, and have been using JavaScript for many years, so I am very familiar with it.  But it is still newish to me compared to ~10 years of work in PHP, and I find it fun to use.  I had never used the jade templating language either, which I found to be a very nice addition.

The file app.js contains the logic for the server side of the front end.  There are only two routes (aside from static files):  [/](http://162.209.99.212:3000/) and [/get-businesses](http://162.209.99.212:3000/get-businesses).  The first route shows the data and the second returns json results based on selected filters.

I used the jquery [DataTables plugin](https://datatables.net/) to display the results, which I just stumbled across and thought looked neat.  This made it very easy to display the data in a useful, tabular format.  I used LESS as my CSS preprocessor (though I have used SCSS plenty as well).  I created filters for each field of data that seemed relevant to me (though I could easily add more for things like score, etc.)

Finally, I added a quick link at the top to set the filters to show the top 10 Thai restaurants with an inspection grade of B or above.  The results are sorted first by grade and then by score, so if there are two restaurants with a grade of A, the one with a higher score will appear first.

### Technology ###
I am aware the directions suggested using a free hosting service like heroku, however, given the different languages I used in this project, I figured sticking the code up on a VPS (that I already had) was the easiest solution.  I'm happy to rehost elsewhere if that is desirable.  I've used heroku to host RoR projects in the past, though I have not had the chance to use GAE yet.  I am very familiar with the AWS stack.

I normally prefer to host on github, however, I used Bitbucket because github has a 100mb max file size, and the data csv is larger than 100mb.

### Conclusions ###

All in all I enjoyed doing this project.  It was fun to play with a dataset that has such a large impact on my day-to-day life, and since I've showed this to some friends they've started playing with it to see how their favorite restaurants fare.  I did run into some issues with bad data in the original file, however, I always handled this by showing things like "NONE" or "N/A" if the field did not hold an appropriate value.  This sort of thing is not unexpected in a municipal data set, especially one this large.

Thanks for the opportunity to work on this and I'm looking forward to hearing your feedback!